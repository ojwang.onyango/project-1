#include <iostream>     // Used for cin and cout
#include <iomanip>      // Used for formatting cout statements
#include <string>       // Used for string data types
#include <cstdlib>      // Used for rand()
#include <ctime>        // time used to seed random number generator
using namespace std;

void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location );

int main()
{
    /******************************************************************* INITIALIZE VARIABLES */
    bool done = false;                      // Flag for if the game is over
    bool successfulAction = false;          // Flag for if the user entered valid input
    int food = 5;                           // Player stat - how much food they have
    int maxHealth = 20;                     // Player stat - their maximum amount of health
    int health = maxHealth;                 // Player stat - their current health amount
    string name = "ME";                     // Player stat - what their name is
    string location = "Overland Park";      // Player stat - what location they're in
    int day = 1;                            // Player stat - how many days it has been

    string dump;                            // A variable to dump input into
    int choice;                             // A variable to dump input into
    int mod;                                // A variable used for calculations later

    srand( time( NULL ) );                  // Seeding the random number generator

    /******************************************************************* GAME START */
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline( cin, name );

    cout << endl;

    /******************************************************************* GAME LOOP */
    while ( !done )
    {
        /** Start of the round **/
        successfulAction = false;
        DisplayMenu( day, food, health, maxHealth, name, location );
        cout << "CHOICE: ";
        cin >> choice;

        cout << "----------------------------------------" << endl;

        cout << "The day passes: " << day;
        day++;

        if ( food > 0 )                     // Daily events
        {
            cout << endl << "You eat a meal: " << food << endl;
            food--;
            health++;
        }
        else
        {
            mod = rand() % 5 + 1;
            cout << endl << "You are starving! " "(-" << mod << " health)" << endl;
            health -= mod;
        }

        cout << "----------------------------------------" << endl;

        // BINDING FOOD AND HEALTH

        if ( food <= 0 )
        {
            food = 0;
        }

        if ( health > maxHealth )
        {
            health = maxHealth;
        }

        // ENDING EVENTS MESSAGES

        if ( health <= 0 )
        {
            cout << endl << "> " << "You have died!" << endl;

            done = true;
        }
        else if ( day == 20)
        {
            cout << endl << "> " << "In the morning, a group of scavengers find you" << endl;
            cout << endl << "> " << "You agree to live in their town" << endl;

            done = true;
        }

        // SCAVENGING
        if ( choice == 1 )
        {
            cout << "----------------------------------------" << endl;
            cout << " You scavenge here." << endl;
            successfulAction = true;
            int randomChance = rand() % 5;

                if ( randomChance == 0 )
                {
                mod = rand() % 5 + 2;
                cin >> mod;
                cout << endl << "You find a stash of food! " "(+" << mod << " food)" << endl;
                food += mod;
                cout << "----------------------------------------" << endl;
                }

                else if (  randomChance == 1 )
                {
                mod = rand() % 8 + 2;
                cin >> mod;
                cout << endl << "A zombie surprises you!" << endl;
                cout << endl << "You get hurt in the encounter. " "(-" << mod << " health)" << endl;
                health -= mod;
                }

               else if (  randomChance == 2 )
                {
                mod = rand() % 5 + 2;
                cin >> mod;
                cout << endl << "You find some medical supplies. " "(+" << mod << " health)" << endl;
                health -= mod;
                }

              else if (  randomChance == 3 )
                {
                mod = rand() % 6 + 2;
                cin >> mod;
                cout << endl << "A zombie surprises you!" << endl;
                cout << endl << "They take some supplies from you " "(-" << mod << " food)" << endl;
                food -= mod;
                }

                else if ( randomChance == 4 )
                {
                cout << ">" << " You scavenge here." << endl;
                cout << endl << "You don't find anything" << endl;
                }
        }


        //MOVING
        else if (choice == 2 )
        {
            cout << "Walk to where? "   << endl << endl;
            cout << "1. Overland Park"  << endl;
            cout << "2. Raytown "       << endl;
            cout << "3. Kansas city"    << endl;
            cout << "4. Gladstone"      << endl;
            cin >> choice;

            if ( choice == 1 && location == "Overland Park")
            {
            cout << endl << "You're already there!" << endl;
            }
                if ( choice = 1 && location != "Overland Park" )
                {
                location = "Overland Park";
                successfulAction = true;
                }

           else if ( choice == 2 && location == "Raytown")
            {
            cout << endl << "You're already there!" << endl;
            }
                if ( choice = 2 && location != "Raytown" )
                {
                location = "Raytown";
                successfulAction = true;
                }

            else if ( choice == 3 && location == "Kansas City")
            {
            cout << endl << "You're already there!" << endl;
            }
                if ( choice = 3 && location != "Kansas City" )
                {
                location = "Raytown";
                successfulAction = true;
                }

           else if ( choice == 4 && location == "Gladstone")
            {
            cout << endl << "You're already there!" << endl;
            }
                if ( choice = 4 && location != "Gladstone" )
                {
                location = "Gladstone";
                successfulAction = true;
                }

            else
            {
                 cout << "Invalid selection!" << endl;
            }

        }



        // MOVING OUTCOMES
        if ( successfulAction = true )
        {
           // cout << "You travel to " << location << endl;
            int randomChance = rand() % 5;
            cin >> randomChance;

                if ( randomChance == 0 )
                {
                cout << "A zombie attacks!";
                cout << "You fight it off. " << endl;
                }

               else if (  randomChance == 1 )
                {
                mod = rand() % 5 + 2;
                cin >> mod;
                cout << "A zombie attacks!";
                cout << endl << "It bites you as you fight it! "  "(-" << mod << " health)" << endl;
                health -= mod;
                }

              else if (  randomChance == 2 )
                {
                mod = rand() % 5 + 1;
                cin >> mod;
                cout << endl << "You find another scavenger and trade goods. " "(+" << mod << " food)" << endl;
                food += mod;
                }

              else if (  randomChance == 3 )
                {
                mod = rand() % 5 + 2;
                cin >> mod;
                cout << endl << "You find a safe building to rest in temporarily " "(+" << mod << " health)" << endl;
                health += mod;
                }

               else if ( randomChance == 4 )
                {
                cout << endl << "The journey is uneventful" << endl;
                }
        }

            else
            {
                continue;
            }


        /** End of the round - nothing to update **/
        cout << endl << "Press ENTER to continue...";
        cin.ignore();
        getline( cin, dump );

        cout << "----------------------------------------" << endl;
        cout << "----------------------------------------" << endl;
    }

    /******************************************************************* GAME OVER */
    cout << endl << endl << "You survived the apocalpyse on your own for " << day << " days." << endl;

    return 0;
}

/* You don't need to modify this, it just displays the user's information at the start of each round. */
void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location )
{
    cout << "----------------------------------------" << endl;
    cout << left << setw( 3 ) << "-- " << setw( 35 ) << name << "--" << endl;
    cout << left
        << setw( 6 ) << "-- Day "   << setw( 4 ) << day
        << setw( 6 ) << "Food: "    << setw( 4 ) << food
        << setw( 8 ) << "Health: "  << setw( 2 ) << health << setw( 1 ) << "/" << setw( 2 ) << maxHealth
        << right << setw( 6 ) << "--" << endl;
    cout << left
        << setw( 10 ) << "-- Location: " << setw( 20 ) << location
        << right << setw( 7 ) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elseware                 --" << endl;
    cout << "----------------------------------------" << endl;
    for ( int i = 0; i < 15; i++ )
    {
        cout << endl;
    }
}
